app.controller('home', function($scope, $http){

	var refresh = function(){
		$http.get('http://localhost:3000/categories').success(function(response){
				$scope.categories = response;
				$scope.categorie = "";
		});
		$http.get('http://localhost:3000/articles').success(function(response){
				$scope.articles = response;
				$scope.article = "";
		});
	}
	refresh();

	$scope.addCategorie = function(){
		$http.post('http://localhost:3000/categories', $scope.categorie).success(function(reponse){
			refresh();
		});
	}

	$scope.removeCategorie = function(id){
		$http.delete('http://localhost:3000/categories/' + id).success(function(response){
			refresh();
		});
	}

	$scope.editCategorie = function(id){
		$http.get('http://localhost:3000/categories/' + id).success(function(response){
			$scope.categorie = response;
		});
	}

	$scope.updateCategorie = function(id){
		$http.put('http://localhost:3000/categories/' + id, $scope.categorie).success(function(response){
			$scope.categorie = "";
			refresh();
		});		
	}


	$scope.addArticle = function(){
		$http.post('http://localhost:3000/articles', $scope.article).success(function(reponse){
			refresh();
		});
	}

	$scope.removeArticle = function(id){
		$http.delete('http://localhost:3000/articles/' + id).success(function(response){
			refresh();
		});
	}

	$scope.editArticle = function(id){
		$http.get('http://localhost:3000/articles/' + id).success(function(response){
			$scope.article = response;
		});
	}

	$scope.updateArticle = function(id){
		$http.put('http://localhost:3000/articles/' + id, $scope.article).success(function(response){
			$scope.article = "";
			refresh();
		});		
	}


});