$(document).ready(function(){

	$('#search').on('click', function(){
			$('#search-bar-block').toggleClass('hide');
	});

	$('#wrapper').on('click', function(){
		$('#search-bar-block').addClass('hide');
	})

	$('#search-bar').on('keyup', function(e){
	   if(e.keyCode === 13) {
	     var parameters = { search: $(this).val() };
	       $.get( '/searching',parameters, function(data) {
	       $('#results').html(data);
	     });
	    };
 	});



});