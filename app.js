var http = require('http');
var https = require('https');
var path = require('path');
var request = require('request');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var Twig = require('twig');
var util = require('util');
var newrelic = require('newrelic');
var Oauth = require('oauth');

// TODO gestion des cookies // gestion de la session
var errorHandler = require('errorHandler');
var express = require('express');

// Bootstrap express
var api = express();
var backoffice = express();
var front = express();

api.locals.newrelic = newrelic;
front.locals.newrelic = newrelic;
backoffice.locals.newrelic = newrelic;

// development error handler
// will print stacktrace
if (backoffice.get('env') === 'development') {
  backoffice.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// development error handler
// will print stacktrace
if (front.get('env') === 'development') {
  front.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
backoffice.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

front.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

// Add JSON Support

api.use(bodyParser.urlencoded({ extended: true }));
api.use(bodyParser.json());

backoffice.use(bodyParser.urlencoded({ extended: true }));
backoffice.use(bodyParser.json());
// configure app to use bodyParser()
// this will let us get the data from a POST
front.use(bodyParser.urlencoded({ extended: true }));
front.use(bodyParser.json());

// all environments
// declare port here

var apiPort = 3000;
var backofficePort = 3001;
var frontPort = 3002;

api.set('port', process.env.PORT || apiPort);
backoffice.set('port', process.env.PORT || backofficePort);
front.set('port',  process.env.PORT || frontPort);
/*front.use(favicon());*/

front.use(express.static(path.join(__dirname, 'public')));

// view engine setup
front.set('views', path.join(__dirname, '/public/views'));
/*app.set('view engine', 'jade');*/
front.set('view engine', 'twig');
front.set("view options", { layout: false });
front.set('twig options', {strict_variables: false});


// routes front-office
front.get('/', function(req, res){
	res.render('index.twig');
});

front.get('/searching', function(req, res){
   var val = req.query.search;
   console.log(val);
   res.send(val);
});

front.get('/categories', function(req, res){
	request('http://localhost:3000/categories', function (error, response, body) {
	  	if (!error && response.statusCode == 200) {
			res.render('categories.twig', { categories : JSON.parse(body)});
	  	}
	})
});

front.get('/categories/:categories', function(req, res){
  var categorie = req.params.categories;
  console.log(categorie);
  request('http://localhost:3000/categories', function (error, response, body) {
      if (!error && response.statusCode == 200) {
        res.render('articles.twig', { categorie_title : categorie});
      }
  });
});

// autorise api backoffice à communiquer avec api
backoffice.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:' + apiPort);
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

// autorise api frontoffice à communiquer avec api
front.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:' + apiPort);
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

// autorisation de communiquer entre api et api backoffice
api.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:' + backofficePort || 'http://localhost:' + frontPort);
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
mongoose.connect('mongodb://localhost/portfolio', function(){
});
// verification de la connexion à la base de données
// error
mongoose.connection.on('error', function(){
	console.log('connection failed');
});
// success
mongoose.connection.once('open', function () {
  console.log('connection à la base ... ok');
});

var UserSchema = Schema({
  name: { type: String, default: '' },
  email: { type: String, default: '' },
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  admin: Boolean,
  provider: { type: String, default: '' },
  hashed_password: { type: String, default: '' },
  salt: { type: String, default: '' },
  authToken: { type: String, default: '' },
  facebook: {},
  twitter: {},
  github: {},
  google: {},
  linkedin: {},
  created_at: Date,
  updated_at: Date
});

var CategorieSchema = Schema({
	name : { type : String , default : '', trim : true },
	slug : { type : String , default : '', trim : true },
  author: {
        type : Schema.ObjectId,
        ref: "User"
  },
  articles : [{ type : Schema.ObjectId , ref : "Article"}],
  createdAt : { type: Date, default: Date.now },
  updated_at: Date
});

var ArticleSchema = Schema({
    title: { type : String , required: true, unique : true, trim : true },
    slug: { type : String , default : '', trim : true },
    content: { type : String , default : '', trim : true },
    author: {
        type : Schema.ObjectId,
        ref: "User"
    },
    categorie : {
      type : Schema.ObjectId,
      ref : "Categorie"
    },
    createdAt : { type: Date, default: Date.now },
    updated_at: Date
});

var CommentSchema = Schema({
    text: { type : String , default : '', trim : true },
    article: {
        type : Schema.ObjectId,
        ref: "Article"
    },
    author: {
        type : Schema.ObjectId,
        ref: "User"
    },
    createdAt : { type: Date, default: Date.now },
    updated_at: Date
});

var User = mongoose.model('User', UserSchema);
var Categorie = mongoose.model('Categorie', CategorieSchema);
var Article = mongoose.model('Article', ArticleSchema);
var Comment = mongoose.model('Comment', CommentSchema);

backoffice.use(express.static(__dirname + '/private'));

/*front.get('/', function(req, res){
	res.send('toto');
});*/

api.get('/categories', function(req, res){
	Categorie.find(function(err, categories){
		if(categories.length > 0){
			res.json(categories);
		}else{
			res.json({message : 'pas de catégories'});
		}
	});	
});

api.get('/categories/:id', function(req, res){
	var id = req.params.id;
	Categorie.findById(id, function(err, response){
    if(err){
      console.log('erreur get categories');
    }
		res.send(response);
	});
});

api.post('/categories', function(req, res){
	var name = req.body.name;
	if(name != ''){
		var newCategorie = new Categorie({name : name,
                                      slug : name.toLowerCase()});
		newCategorie.save(function(err){
      if(err){
        console.log('erreur ajout categorie');
      }
			res.json({message : 'insert categorie ok'});
		});
	}
});

api.put('/categories/:id', function(req, res){
	var data = req.body.name;
	var id = req.params.id;
	Categorie.findByIdAndUpdate(id, req.body , function(err){
    if(err){
      console.log('erreur update categorie');
    }
		res.json({message : 'update categorie ok'});
	});
});

api.delete('/categories/:id', function(req, res){
	var id = req.params.id;
	Categorie.findByIdAndRemove(id, function(err){
    if(err){
      console.log('erreur delete categorie');
    }
		res.json({message : 'delete categorie ok'});
	});
});

///////////////////////////////// Gestion des articles

api.get('/articles', function(req, res){
  Article.find(function(err, articles){
    if(articles.length > 0){
      res.json(articles);
    }else{
      res.json({message : 'pas d\'articles...'});
    }
  }); 
});

// on récupère les articles pour une catégorie donnée
api.get('/articlesPop/:name', function(req, res){
  var name = req.params.name;
  console.log(name);
  Categorie.find({name : name}).populate("articles").exec(function(err, articles){
    console.log(articles);
    res.send(articles);
  });
});

api.get('/articles/:id', function(req, res){
  var id = req.params.id;
  Article.findById(id, function(err, response){
    if(err){
      console.log('erreur get articles');
    }
    res.send(response);
  });
});

api.post('/articles', function(req, res){
  var title = req.body.title;
  var content = req.body.content;
  var categorieId = req.body.catId;
  if(title != ''){
    var newArticle = new Article({title : title,
                                  content : content,
                                  categorie: categorieId});
    newArticle.save(function(err){
      if(err){
        console.log('erreur ajout article');
      }
        Categorie.update({ _id : categorieId }, {$push : {articles : newArticle}}, function(err, numAffected){
          res.send('article créé avec liaison dans categorie');
        });
    });
  }
});

api.put('/articles/:id', function(req, res){
  var data = req.body.title;
  var id = req.params.id;
  Article.findByIdAndUpdate(id, req.body , function(err){
    if(err){
      console.log('erreur update article');
    }
    res.json({message : 'update article ok'});
  });
});

api.delete('/articles/:id', function(req, res){
  var id = req.params.id;
  Article.findByIdAndRemove(id, function(err){
    if(err){
      console.log('erreur delete article');
    }
    res.json({message : 'delete article ok'});
  });
});


var frontServer = front.listen(front.get('port'));
var apiServer = api.listen(api.get('port'));
var backServer = backoffice.listen(backoffice.get('port'));

// Chargement de socket.io
var io = require('socket.io').listen(frontServer);

// socket.io events
io.on( "connection", function(socket)
{
    console.log('Un client est connecté !');
    socket.emit('message', 'Vous êtes bien connecté !');
    socket.on('alphabet', function(data){
      console.log(data);
      socket.emit('data-ok');
    });
});
